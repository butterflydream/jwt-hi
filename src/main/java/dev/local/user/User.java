package dev.local.user;

import lombok.Data;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;

@Data
@Entity
public class User {
    @Id
//    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;
    @PrePersist
    private void ensureId(){
        this.setId(UUID.randomUUID().toString());
    }
    @Indexed(unique=true, direction= IndexDirection.DESCENDING, dropDups=true)
    private String username;
    private String password;
    @Indexed(unique=true, direction= IndexDirection.DESCENDING, dropDups=true)
    private String email;
    private Date lastPasswordResetDate;
    private String roles;
}
