package dev.local.secruity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


import dev.local.user.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        JwtUser jwtUser = new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getEmail(),
                mapToGrantedAuthorities(Arrays.asList(user.getRoles().split(","))),
                user.getLastPasswordResetDate()
        );
        return jwtUser;
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<String> authorities) {
        return authorities.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
    public static void main(String[] args){
       List<String> as = Arrays.asList("ROLE_USER","ROLE_ADMIN");
       String ss = as.toString();
    }

}

